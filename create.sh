#!/bin/bash

set -euo pipefail

OPERATOR_NAMESPACE="knative-operator"
SERVING_NAMESPACE="knative-serving"
EVENTING_NAMESPACE="knative-eventing"

# Install knative operator
echo "Installing knative operator..."
kubectl create ns ${OPERATOR_NAMESPACE} --dry-run=client -o yaml | kubectl apply --server-side=true -f -
kubectl kustomize deployment/knative-operator/lan | kubectl apply -f -
kubectl -n ${OPERATOR_NAMESPACE} rollout status deployment knative-operator
echo

# Install knative serving
echo "Installing knative serving..."
kubectl create ns ${SERVING_NAMESPACE} --dry-run=client -o yaml | kubectl apply --server-side=true -f -
kubectl kustomize deployment/knative-serving/lan | kubectl apply -f -
kubectl -n ${SERVING_NAMESPACE} rollout status deployment activator autoscaler controller webhook autoscaler-hpa net-kourier-controller 3scale-kourier-gateway
kubectl -n knative-serving get knativeservings.operator.knative.dev
echo

# Install knative eventing
echo "Installing knative eventing..."
kubectl create ns ${EVENTING_NAMESPACE} --dry-run=client -o yaml | kubectl apply --server-side=true -f -
kubectl kustomize deployment/knative-eventing/lan | kubectl apply -f -
kubectl -n ${EVENTING_NAMESPACE} rollout status deployment eventing-webhook imc-controller eventing-controller imc-dispatcher mt-broker-filter mt-broker-ingress mt-broker-controller
kubectl -n knative-eventing get knativeeventings.operator.knative.dev
echo
