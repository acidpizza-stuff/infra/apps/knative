# KNative

[[_TOC_]]

## Upgrades

- KNative Operator: Download `operator.yaml` from the [github releases page](https://github.com/knative/operator/releases)
- Kourier: Download `kourier.yaml` from [github latest download](https://github.com/knative/net-kourier/releases/latest/download/kourier.yaml)
  - This does not need to be done as installation is handled by knative-serving CR