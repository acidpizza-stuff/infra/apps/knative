#!/bin/bash

# set -euo pipefail

OPERATOR_NAMESPACE="knative-operator"
SERVING_NAMESPACE="knative-serving"
EVENTING_NAMESPACE="knative-eventing"

# Delete knative eventing
echo "Deleting knative eventing..."
kubectl kustomize deployment/knative-eventing/lan | kubectl delete -f -
kubectl create ns ${EVENTING_NAMESPACE} --dry-run=client -o yaml | kubectl delete -f -
echo

# Delete knative serving
echo "Deleting knative serving..."
kubectl kustomize deployment/knative-serving/lan | kubectl delete -f -
kubectl create ns ${SERVING_NAMESPACE} --dry-run=client -o yaml | kubectl delete -f -
echo

# Delete knative operator
echo "Deleting knative operator..."
kubectl kustomize deployment/knative-operator/lan | kubectl delete -f -
kubectl create ns ${OPERATOR_NAMESPACE} --dry-run=client -o yaml | kubectl delete -f -
echo

